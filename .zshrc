export GPG_TTY=$(tty)
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh
export EDITOR='nvim'
export UPDATE_ZSH_DAYS=1
ZSH_THEME="philips"

plugins=(colorize docker git jump rust z)

source $ZSH/oh-my-zsh.sh

# functions
function short() {
    site=$(echo $1 | sed 's/^https\?:\/\///g')
    full='https://'
    full+=$site
    echo $(curl -s -F "shorten=${full}" https://0x0.st)
}		# thanks @tiagotriques
function weather(){
    curl v2.wttr.in/$(echo $@ | sed 's/ /\_/g')
}
# aliases 
alias gcams='git commit -S -a -m'
alias copy='xclip -sel clip'
alias psc='ps xawf -eo pid,user,cgroup,args'
alias l='ls -lh'
alias ll='ls -lah'
